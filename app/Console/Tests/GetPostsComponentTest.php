<?php

use App\Console\Tests\Factories\PostsFactory;
use function Pest\Laravel\artisan;

use Tests\ComponentTestCase;

uses(ComponentTestCase::class);
uses()->group('component');

test("Command users:get-posts-count {user_id} success", function () {
    $userId = 3;

    $postResponse = PostsFactory::new()->makeResponseSearch(['user_id' => $userId]);

    $this->mockPostsApi()
        ->expects('searchPosts')
        ->andReturn($postResponse);

    $count = $postResponse->getMeta()->getPagination()->getTotal();

    artisan("users:get-posts-count {$userId}")->assertSuccessful()->expectsOutput($count);
});
