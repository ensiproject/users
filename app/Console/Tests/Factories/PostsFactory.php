<?php

namespace App\Console\Tests\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use EnsiProject\PostsClient\Dto\SearchPostsResponse;

class PostsFactory extends BaseApiFactory
{

    protected function definition(): array
    {
        return [
            'id' => $this->optionalId(),
            'title' => $this->faker->text(20),
            'body' => $this->faker->text(100),
            'rating' => $this->faker->numberBetween(-1000, 1000),
            'user_id' => $this->faker->numberBetween(1),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    public function makeResponseSearch(array $extra = [], int $count = 1): SearchPostsResponse
    {
        return $this->generateResponseSearch(SearchPostsResponse::class, $extra, $count);
    }
}
