<?php

namespace App\Console\Commands;

use App\Domain\Contents\Actions\GetPostsCountByUserIdAction;
use Illuminate\Console\Command;

class GetPostsCount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:get-posts-count {user_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get posts count by user_id';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(GetPostsCountByUserIdAction $action)
    {
        $userId = $this->argument('user_id');

        if (empty($userId)) {
            $this->error('User id is empty');
            return Command::INVALID;
        }

        $result = $action->execute($userId);

        $this->info($result);
        $this->info('Successful!');
        return Command::SUCCESS;
    }
}
