<?php

namespace App\Http\ApiV1\Support\Tests\Factories;

use Ensi\TestFactories\Factory;

abstract class BaseApiFactory extends Factory
{
    public ?int $id = null;

    public function withId(?int $id = null): static
    {
        return $this->immutableSet('id', $id ?? $this->faker->randomNumber());
    }

    protected function optionalId()
    {
        return $this->whenNotNull($this->id, $this->id);
    }

    /**
     * @template T
     * @param class-string<T> $classResponse
     * @param array $extra
     * @param int $count
     * @return T
     */
    protected function generateResponseSearch(string $classResponse, array $extra = [], int $count = 1)
    {
        $meta = $classResponse::openAPITypes()['meta'];

        $data = [];
        for ($i = 1; $i <= $count; $i++) {
            $data[] = $this->make($this->makeArray($extra));
        }

        return new $classResponse([
            'data' => $data,
            'meta' => new $meta([
                'pagination' => PaginationFactory::new()->makeOffsetByClass($meta::openAPITypes()['pagination']),
            ]),
        ]);
    }
}
