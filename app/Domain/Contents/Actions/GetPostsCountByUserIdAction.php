<?php

namespace App\Domain\Contents\Actions;

use EnsiProject\PostsClient\Api\PostsApi;
use EnsiProject\PostsClient\ApiException;
use EnsiProject\PostsClient\Dto\RequestBodyPagination;
use EnsiProject\PostsClient\Dto\SearchPostsRequest;

class GetPostsCountByUserIdAction
{
    public function __construct(private readonly PostsApi $postsApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $userId): ?int
    {
        $searchPostsRequest = new SearchPostsRequest();
        $searchPostsRequest->setFilter((object)[
            'user_id' => $userId,
        ]);
        $searchPostsRequest->setPagination(
            new RequestBodyPagination(['limit' => 0])
        );

        $postsMeta = $this->postsApi->searchPosts($searchPostsRequest)->getMeta();

        return $postsMeta->getPagination()->getTotal();
    }
}
