<?php

return [
    'ensiproject' => [
        'posts' => [
            'base_uri' => env('POSTS_SERVICE_HOST') . "/api/v1",
        ],
    ],
];
