<?php

namespace Tests;

use EnsiProject\PostsClient\Api\PostsApi;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Mockery\MockInterface;

class ComponentTestCase extends TestCase
{
    use DatabaseTransactions;

    protected function mockPostsApi(): MockInterface|PostsApi
    {
        return $this->mock(PostsApi::class);
    }
}
